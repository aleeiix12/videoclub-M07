<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CatalogController extends Controller
{
    public function catalog()
    {
        return view('catalog.index');
    }

    public function showById($id)
    {
        return view('catalog.show', compact('id'));

    }

    public function createPelicula()
    {
        return view('catalog.create');
    }

    public function editPelicula($id)
    {
        return view('catalog.edit', compact('id'));
    }
}
